<?php

if ( !class_exists( 'StardustThemeLocale' ) ) {
	class StardustThemeLocale {
		function __construct() {
			add_action('after_setup_theme', array($this, 'setup_locale'), 0);
		}

		public function setup_locale() {
			global $star_language, $star_locales;

			if (is_admin()) {
				return;
			}

			switch_to_locale($star_locales[$star_language]);
			//			add_filter('locale', function() {
			//				global $star_language, $star_locales;
			//				return $star_locales[ $star_language ];
			//			});
			load_theme_textdomain('stardust-theme', get_template_directory() . '/languages');
		}
	}
}