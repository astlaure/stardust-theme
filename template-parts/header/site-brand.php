<?php

global $star_language;
global $star_default_language;
global $star_supported_languages;

$locale_path = $star_language === $star_default_language ? '/' : $star_language . '/';

if ( has_custom_logo() ): ?>
    <a href="<?php echo home_url( $locale_path ) ?>">
        <?php echo wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full' ) ?>
    </a>
<?php else: ?>
    <a href="<?php echo home_url( $locale_path ) ?>">
        <?php bloginfo( 'name' ); ?>
    </a>
<?php endif; ?>
