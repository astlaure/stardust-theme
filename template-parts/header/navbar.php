<?php global $star_language; ?>

<nav class="navbar">
    <div class="container">

        <div class="nav-brand">
	        <?php get_template_part( 'template-parts/header/site-brand' ) ?>
        </div>

        <div class="nav-menu">
            <div class="nav-socials">
	            <?php
	            if ( is_active_sidebar( 'sidebar-socials-' . $star_language ) ):
		            dynamic_sidebar( 'sidebar-socials-' . $star_language );
	            endif;
	            ?>
                <button type="button" class="nav-burger" onclick="window.toggleMobileMenu();">
                    <i></i>
                    <i></i>
                    <i></i>
                </button>
            </div>
            <div class="nav-menu-items">
	            <?php wp_nav_menu(
                    array(
                        'theme_location' => 'primary_menu_' . $star_language,
                        'menu_class' => 'wp-primary-menu',
                        'container' => false
                    )
                ) ?>
            </div>
        </div>
    </div>
</nav>