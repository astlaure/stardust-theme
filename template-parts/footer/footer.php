<?php global $star_language; ?>

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col medium-4">
				<?php get_template_part( 'template-parts/footer/site-brand' ) ?>
			</div>
			<div class="col medium-4">
				<?php
				if ( is_active_sidebar( 'sidebar-footer-' . $star_language ) ):
					dynamic_sidebar( 'sidebar-footer-' . $star_language );
				endif;
				?>
			</div>
			<div class="col medium-4">
				<?php echo do_shortcode('[contact-form]') ?>
			</div>
		</div>
		<div class="copyright">
			<p>Copyright Alexandre St-Laurent 2022</p>
		</div>
	</div>
</footer>
