<article>
	<h2><?php the_title() ?></h2>
	<div class="metadata">
		<?php the_author_link(); ?>
		<?php the_category( ' ' ); ?>
		<p><?php the_date() ?></p>
	</div>
    <div>
        <?php the_content(); ?>
    </div>
</article>