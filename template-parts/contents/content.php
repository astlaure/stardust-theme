<article <?php post_class();?>>
	<?php
	if ( has_post_thumbnail() ):
		the_post_thumbnail( 'full' );
	endif;
	?>
	<h2><?php the_title(); ?></h2>
	<p><?php the_excerpt(); ?></p>
	<a href="<?php the_permalink(); ?>"><?php _e( 'Read More', 'stardust-theme' ); ?></a>
</article>