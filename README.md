## Landing page

1. Create a page Accueil and a page Homepage
2. Add the front-page.php to the root of the theme to activate it

![Setup Landing Page!](./docs/setup_landing_page.png "Setup")

## Installation steps

1. Install the fr_CA language pack
   1. Settings -> General -> Site Languages
   2. Select Francais Canada and save
   3. Return to English Canada
2. Setup the Accueil and Homepage pages

## Fallbacks

1. The sidebar templates doesn't work

## TODO

1. try to fix sidebar templates
2. create all the labels for the new pages
3. try to build a website
4. check for emails
5. check for forms

## Technical Debts

1. MiniCssExtractPlugin is stuck at 2.4.7 because of a bug
2. remove the sass folder when everything is converted again