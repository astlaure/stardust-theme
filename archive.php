<?php get_header(); ?>

<img
    class="img-fluid"
    src="<?php header_image(); ?>"
    height="<?php echo get_custom_header()->height; ?>"
    width="<?php echo get_custom_header()->width; ?>"
    alt=""
>

<div class="container">
    <h1>Hello Index Bootstrap</h1>

    <div class="services">
        <h1>Out Services</h1>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4"></div>
        </div>
    </div>

    <?php

    the_archive_title( '<h1 class="archive-title">', '</h1>' );
    the_archive_description();

    if ( have_posts() ):
        while ( have_posts() ): the_post();
            get_template_part( 'template-parts/contents/content', 'archive' );
        endwhile;
    ?>

        <div class="row">
            <div>
                <?php previous_posts_link( "<< Newer posts" ); ?>
            </div>
            <div>
                <?php next_posts_link( 'Older posts >>' ); ?>
            </div>
        </div>

    <?php
    else:
        ?>
        <p>Wrong page</p>
        <?php
    endif;
    ?>

</div>

<div>
    <?php get_sidebar( 'blog' ); ?>
</div>

<?php get_footer(); ?>