<?php get_header() ?>

<main>
	<?php if ( have_posts() ):
		while ( have_posts() ): the_post();
			get_template_part( 'template-parts/contents/content', 'single' );
		endwhile;
	endif;
	?>

    <div class="pagination">
        <?php
            next_post_link();
		    previous_post_link();
        ?>
    </div>
</main>

<?php get_footer(); ?>
