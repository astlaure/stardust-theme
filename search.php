<?php get_header(); ?>

<img
    class="img-fluid"
    src="<?php header_image(); ?>"
    height="<?php echo get_custom_header()->height; ?>"
    width="<?php echo get_custom_header()->width; ?>"
    alt=""
>

<div class="container">
    <h1>Hello Index Bootstrap</h1>

    <div class="services">
        <h1>Out Services</h1>
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4"></div>
            <div class="col-sm-4"></div>
        </div>
    </div>

    <h2>Search results for: <?php echo get_search_query(); ?></h2>

    <?php

    get_search_form();

    if ( have_posts() ):
        while ( have_posts() ): the_post();
            get_template_part( 'template-parts/contents/content', 'search' );
        endwhile;
    else:
        ?>
        <p>Wrong page</p>
        <?php
    endif;

    the_posts_pagination( array(
        'prev_text' => 'Previous',
        'next_text' => 'Next',
    ) );
    ?>

</div>

<div>
    <?php get_sidebar( 'blog' ); ?>
</div>

<?php get_footer(); ?>