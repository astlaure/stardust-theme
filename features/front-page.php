<?php
global $star_language;
$star_page = $star_language === 'fr' ? 'accueil' : 'homepage';

?>

<?php get_header() ?>

<main>
    <?php
    $query = new WP_Query(
        array( 'pagename' => $star_page, 'meta_key' => 'language', 'meta_value' => $star_language )
    );
    if ( $query->have_posts() ):
        while ( $query->have_posts() ): $query->the_post();
            get_template_part( 'template-parts/contents/content', 'page' );
        endwhile;
        wp_reset_postdata();
    endif;
    ?>
</main>

<?php get_footer(); ?>
