window.toggleMobileMenu = () => {
    const items = document.querySelector('.navbar .nav-menu-items');

    if (items) {
        items.classList.toggle('visible');
    }
};