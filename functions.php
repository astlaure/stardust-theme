<?php

if (!class_exists('StardustTheme')) {
	class StardustTheme {
		function __construct() {
            add_action( 'after_switch_theme', array( $this, 'after_switch_theme' ), 10, 2 );
			add_action('after_setup_theme', array($this, 'after_setup_theme'), 0);
			add_action('widgets_init', array($this, 'register_sidebars'));
			add_action('wp_enqueue_scripts', array($this, 'register_assets'), 9999);
            add_action( 'admin_init', array( $this, 'admin_init' ) );

			add_shortcode( 'contact-form', array( $this, 'display_contact_form' ) );

			require_once __DIR__ . '/classes/locale.php';
			new StardustThemeLocale();
		}

        public function admin_init() {
	        add_editor_style('assets/css/app.css');
        }

		public function add_theme_support() {
			//			$args = array(
			//				'height' => 225,
			//				'width' => 1920,
			//			);
			//			add_theme_support( 'custom-header', $args );

			add_theme_support('post-thumbnails');

			add_theme_support('title-tag');

			add_theme_support('post-formats', array('video', 'image'));

			add_theme_support('custom-logo', array(
				'height' => 110,
				'width' => 200,
				'flex-width' => true,
				'flex-height' => true
			));

			// GUTENBERG
			//			add_theme_support( 'editor-color-palette', array(
			//				array(
			//					'name' => __( 'Blood Red', 'theme-devel' ),
			//					'slug' => 'blood-red',
			//					'color' => '#b9121b'
			//				),
			//				array(
			//					'name' => __( 'White', 'theme-devel' ),
			//					'slug' => 'white',
			//					'color' => '#ffffff'
			//				)
			//			) );
			//			add_theme_support( 'disable-custom-colors' );

			add_theme_support('align-wide');
			add_theme_support('responsive-embeds');

			// add_theme_support( 'wp-block-styles' );
			add_theme_support('editor-styles');
//			add_editor_style('assets/css/app.css');
		}

		public function register_menus() {
			global $star_supported_languages;

			$menus = array();

			foreach ($star_supported_languages as $language) {
				$menus['primary_menu_' . $language] = __('Primary Menu ' . strtoupper($language), 'stardust-theme');
				$menus['footer_menu_' . $language] = __('Footer Menu ' . strtoupper($language), 'stardust-theme');
			}

			register_nav_menus($menus);
		}

		public function after_setup_theme() {
			$this->add_theme_support();
			$this->register_menus();
		}

		public function register_sidebars() {
			global $star_supported_languages;

			foreach ($star_supported_languages as $language) {
//				register_sidebar(array(
//					'name' => 'Home Page ' . strtoupper($language) . ' Sidebar',
//					'id' => 'sidebar-' . $language,
//					'description' => 'This is the home ' . $language . ' page sidebar. You can add your widgets here.',
//					'before_widget' => '<div class="widget-wrapper">',
//					'after_widget' => '</div>',
//					'before_title' => '<h2 class="widget-title">',
//					'after_title' => '</h2>',
//				));

				register_sidebar(array(
					'name' => 'Social ' . strtoupper($language) . ' Sidebar',
					'id' => 'sidebar-socials-' . $language,
					'description' => 'This is the ' . $language . ' socials sidebar. You can add your widgets here.',
					'before_widget' => '<div class="widget-wrapper">',
					'after_widget' => '</div>',
					'before_title' => '<h2 class="widget-title">',
					'after_title' => '</h2>',
				));

				register_sidebar(array(
					'name' => 'Footer ' . strtoupper($language) . ' Sidebar',
					'id' => 'sidebar-footer-' . $language,
					'description' => 'This is the ' . $language . ' footer sidebar. You can add your widgets here.',
					'before_widget' => '<div class="widget-wrapper">',
					'after_widget' => '</div>',
					'before_title' => '<h2 class="widget-title">',
					'after_title' => '</h2>',
				));
			}
		}

		public function register_assets() {
			wp_enqueue_style('stardust', get_template_directory_uri() . '/assets/css/app.css', array(), '1.0.0');

			wp_enqueue_script('stardust', get_template_directory_uri() . '/assets/js/app.js', array(), '1.0.0', true);
		}

		public function after_switch_theme( $old_name, $old_theme ) {
			require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'stardust-i18n/stardust-i18n.php' ) ) {
				switch_theme( $old_theme->stylesheet );
				add_action( 'admin_notices', array( $this, 'error_notice' ) );
				return false;
			}
		}

		public function error_notice() {
			?>
			<div class="notice notice-error">
				<p>Activate the Stardust I18n first.</p>
			</div>
			<?php
		}

        public function display_contact_form() {
	        ob_start();
	        require __DIR__ . '/shortcodes/contact-form.php';
	        return ob_get_clean();
        }
	}
}

if (class_exists('StardustTheme')) {
	new StardustTheme();
}