<?php
/*
Template Name: Container
*/
?>

<?php get_header() ?>

<main>
    <div class="container">
        <?php if (have_posts()) :
            while (have_posts()) : the_post();
                get_template_part('template-parts/contents/content', 'page');
            endwhile;
        endif;
        ?>
    </div>
</main>

<?php get_footer(); ?>