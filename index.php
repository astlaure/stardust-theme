<?php get_header() ?>

<main>
	<?php
	if ( have_posts() ):
		while ( have_posts() ): the_post();
			get_template_part( 'template-parts/contents/content', get_post_type() );
		endwhile;
    else:
        get_template_part( 'template-parts/contents/content', 'none' );
	endif;
	?>
</main>

<?php get_footer() ?>
